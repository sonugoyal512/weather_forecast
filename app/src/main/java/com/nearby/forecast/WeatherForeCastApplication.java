package com.nearby.forecast;

import android.app.Application;
import android.location.Geocoder;

import java.util.Locale;

public class WeatherForeCastApplication extends Application {

    private static Geocoder geocoder;

    @Override
    public void onCreate() {
        super.onCreate();
        geocoder = new Geocoder(this, Locale.getDefault());
    }

    public static Geocoder getGeocoderInstance() {
        return geocoder;
    }

}
