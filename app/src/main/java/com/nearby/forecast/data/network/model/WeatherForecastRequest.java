package com.nearby.forecast.data.network.model;

public class WeatherForecastRequest {
    int numberOfDays;
    String city;
    String key;

    public int getNumberOfDays() {
        return numberOfDays;
    }

    public String getCity() {
        return city;
    }

    public String getKey() {
        return key;
    }

    public void setNumberOfDays(int numberOfDays) {
        this.numberOfDays = numberOfDays;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
