package com.nearby.forecast.data.network;

public final class ApiEndPoint {

    public static final String BASE_URL = "https://api.apixu.com/v1/forecast.json/";

    private ApiEndPoint() {
        // This class is not publicly instantiable
    }

}
