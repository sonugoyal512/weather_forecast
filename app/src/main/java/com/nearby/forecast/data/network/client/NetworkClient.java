package com.nearby.forecast.data.network.client;

import com.nearby.forecast.data.network.ApiEndPoint;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NetworkClient {

    private ClientService clientService;

    public NetworkClient() {
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(ApiEndPoint.BASE_URL)
                .addConverterFactory(GsonConverterFactory.create())
                .build();
        clientService = retrofit.create(ClientService.class);
    }

    public ClientService getClientService() {
        return clientService;
    }
}
