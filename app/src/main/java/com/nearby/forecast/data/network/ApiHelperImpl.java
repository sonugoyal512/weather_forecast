package com.nearby.forecast.data.network;

import com.nearby.forecast.data.network.client.NetworkClient;
import com.nearby.forecast.data.network.model.WeatherForecastRequest;
import com.nearby.forecast.data.network.model.WeatherForecastResponse;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class ApiHelperImpl implements ApiHelper {

    private NetworkClient networkClient;
    private static ApiHelperImpl apiHelper;

    private ApiHelperImpl() {
        this.networkClient = new NetworkClient();
    }

    public static ApiHelperImpl getApiHelper() {
        if (apiHelper == null) {
            synchronized (ApiHelperImpl.class) {
                if (apiHelper == null) {
                    apiHelper = new ApiHelperImpl();
                }
            }
        }

        return apiHelper;
    }

    @Override
    public void fetchWeatherForecast(WeatherForecastRequest weatherForecastRequest, final Callback callback) {
        Call<WeatherForecastResponse> weatherCall = networkClient.getClientService().weatherForecast(
                weatherForecastRequest.getKey(),
                weatherForecastRequest.getCity(),
                weatherForecastRequest.getNumberOfDays());
        weatherCall.enqueue(new Callback<WeatherForecastResponse>() {
            @Override
            public void onResponse(Call<WeatherForecastResponse> call, Response<WeatherForecastResponse> response) {
                callback.onResponse(call, response);
            }

            @Override
            public void onFailure(Call<WeatherForecastResponse> call, Throwable t) {
                callback.onFailure(call, t);
            }
        });
    }
}
