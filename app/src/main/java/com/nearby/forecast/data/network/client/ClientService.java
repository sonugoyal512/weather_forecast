package com.nearby.forecast.data.network.client;

import com.nearby.forecast.data.network.model.WeatherForecastResponse;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface ClientService {
    @GET(".")
    Call<WeatherForecastResponse> weatherForecast(@Query("key") String key,
                                                  @Query("q") String city,
                                                  @Query("days") int days);
}
