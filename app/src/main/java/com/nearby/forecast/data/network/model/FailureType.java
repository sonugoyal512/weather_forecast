package com.nearby.forecast.data.network.model;

public enum FailureType {

    LOCATION,
    NETWORK
}
