package com.nearby.forecast.data.network;

import com.nearby.forecast.data.network.model.WeatherForecastRequest;

import retrofit2.Callback;

public interface ApiHelper {

    void fetchWeatherForecast(WeatherForecastRequest weatherForecastRequest, Callback callback);

}
