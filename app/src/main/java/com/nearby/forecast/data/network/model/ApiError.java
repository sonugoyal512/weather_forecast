package com.nearby.forecast.data.network.model;

public class ApiError {

    Error error;

    class Error {
        int code;
        String message;
    }
}
