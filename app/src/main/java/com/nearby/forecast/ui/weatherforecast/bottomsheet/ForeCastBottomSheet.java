package com.nearby.forecast.ui.weatherforecast.bottomsheet;

import android.app.Dialog;
import android.content.DialogInterface;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.BottomSheetBehavior;
import android.support.design.widget.BottomSheetDialog;
import android.support.design.widget.BottomSheetDialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;

import com.nearby.forecast.R;
import com.nearby.forecast.config.UIConfig;
import com.nearby.forecast.constants.AppConstants;
import com.nearby.forecast.data.network.model.WeatherForecastResponse;
import com.nearby.forecast.ui.weatherforecast.adapter.WeatherForeCastAdapter;
import com.nearby.forecast.utils.Utils;

import java.util.ArrayList;
import java.util.List;

public class ForeCastBottomSheet extends BottomSheetDialogFragment {

    private RecyclerView dayForecastRV;
    private WeatherForecastResponse weatherForecastResponse;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        weatherForecastResponse = (WeatherForecastResponse) getArguments().getSerializable(AppConstants.FORECAST_JSON);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View contentView = View.inflate(getContext(), R.layout.bs_weather_forecast, container);
        //handleBottomSheetBehavior(contentView);
        dayForecastRV = contentView.findViewById(R.id.rv_day_forecast);
        decorateRecyclerView();
        return contentView;
    }


    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        BottomSheetDialog dialog = (BottomSheetDialog) super.onCreateDialog(savedInstanceState);
        dialog.setOnShowListener(new DialogInterface.OnShowListener() {
            @Override
            public void onShow(DialogInterface dialog) {
                BottomSheetDialog d = (BottomSheetDialog) dialog;
                FrameLayout bottomSheet = d.findViewById(android.support.design.R.id.design_bottom_sheet);
                if (bottomSheet != null)
                    handleBottomSheetBehavior(BottomSheetBehavior.from(bottomSheet));

            }
        });
        dialog.getWindow().setBackgroundDrawable(new ColorDrawable(Color.TRANSPARENT));
        return dialog;
    }

    private void decorateRecyclerView() {
        dayForecastRV.setLayoutManager(new LinearLayoutManager(getActivity(), LinearLayoutManager.VERTICAL, false));
        WeatherForeCastAdapter weatherForeCastAdapter = new WeatherForeCastAdapter(getActivity(), getForeCastDaysToBeShownOnUI(weatherForecastResponse.getForecast().getForecastday()));
        dayForecastRV.setAdapter(weatherForeCastAdapter);
    }

    private List<WeatherForecastResponse.Forecastday> getForeCastDaysToBeShownOnUI(List<WeatherForecastResponse.Forecastday> forecastdays) {
        List<WeatherForecastResponse.Forecastday> _foreList = new ArrayList<>();
        int size = forecastdays.size();
        for (int i = 1; i < size && i <= UIConfig.NUMBER_OF_TO_BE_SHOWN; i++) {
            _foreList.add(forecastdays.get(i));
        }

        return _foreList;
    }


    protected void handleBottomSheetBehavior(BottomSheetBehavior behavior) {
        if (behavior != null && behavior instanceof BottomSheetBehavior) {
            BottomSheetBehavior bottomSheetBehavior = ((BottomSheetBehavior) behavior);
            bottomSheetBehavior.setPeekHeight(Math.round(Utils.convertPixelsToDp(360.0f, getContext())));
            bottomSheetBehavior.setState(BottomSheetBehavior.STATE_EXPANDED);
            bottomSheetBehavior.setBottomSheetCallback(new BottomSheetBehavior.BottomSheetCallback() {

                @Override
                public void onStateChanged(@NonNull View bottomSheet, int newState) {
                    if (newState == BottomSheetBehavior.STATE_HIDDEN) {
                        dismiss();
                    }
                }

                @Override
                public void onSlide(@NonNull View bottomSheet, float slideOffset) {
                }
            });
        }
    }


}
