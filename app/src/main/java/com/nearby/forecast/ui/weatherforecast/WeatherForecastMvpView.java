package com.nearby.forecast.ui.weatherforecast;

import com.nearby.forecast.data.network.model.FailureType;
import com.nearby.forecast.data.network.model.WeatherForecastResponse;
import com.nearby.forecast.ui.base.MvpView;

public interface WeatherForecastMvpView extends MvpView {

    void showLoader();

    void hideFailureLoader(FailureType failureType);

    void hideSuccessLoader();

    void updateTemp(String temp);

    void updateCity(String city);

    //This is tightly coupled with weatherResponse It could be more generic but due to time constraint implementing this
    void showBottomSheet(WeatherForecastResponse weatherForecastResponse);

    void updateLocationProgress(String text);

    void dismissLocationProgress();

    void invokeLocationManager();
}
