package com.nearby.forecast.ui.base;

import android.location.Location;

import com.nearby.forecast.locationmanager.base.LocationBaseActivity;
import com.nearby.forecast.locationmanager.configuration.Configurations;
import com.nearby.forecast.locationmanager.configuration.LocationConfiguration;

/**
 * Common method can be incorporated here
 */
public class BaseActivity extends LocationBaseActivity {

    @Override
    public LocationConfiguration getLocationConfiguration() {
        return Configurations.defaultConfiguration("Give me the permission!", "Would you mind to turn GPS on?");
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onLocationFailed(int type) {

    }
}
