package com.nearby.forecast.ui.weatherforecast;

import android.location.Location;

import com.nearby.forecast.locationmanager.constants.ProcessType;
import com.nearby.forecast.ui.base.MvpPresenter;

public interface WeatherForecastMvpPresenter<V extends WeatherForecastMvpView> extends MvpPresenter<V> {

    void retryClick();

    void bootStrap();

    void onLocationChanged(Location location);

    void onLocationFailed(int type);

    void onProcessTypeChanged(@ProcessType int newProcess);
}
