package com.nearby.forecast.ui.weatherforecast.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.nearby.forecast.R;
import com.nearby.forecast.data.network.model.WeatherForecastResponse;
import com.nearby.forecast.utils.DateUtils;

import java.util.ArrayList;
import java.util.List;

public class WeatherForeCastAdapter extends
        RecyclerView.Adapter<WeatherForeCastAdapter.WeatherForeCastViewHolder> {

    private Context mContext;
    private List<WeatherForecastResponse.Forecastday> forecastdays = new ArrayList<>();

    public WeatherForeCastAdapter(Context context, List<WeatherForecastResponse.Forecastday> forecastdays) {
        mContext = context;
        this.forecastdays.addAll(forecastdays);
    }

    @Override
    public WeatherForeCastViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.row_weather_forecast, parent, false);
        WeatherForeCastViewHolder viewHolder = new WeatherForeCastViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(WeatherForeCastViewHolder holder, int position) {
        holder.tempTV.setText(forecastdays.get(position).getDay().getAvgtempC() + " C");
        holder.dayTV.setText(DateUtils.getWeekDay(forecastdays.get(position).getDateEpoch()));

    }

    @Override
    public int getItemCount() {
        return forecastdays.size();
    }

    public class WeatherForeCastViewHolder extends RecyclerView.ViewHolder {

        private TextView dayTV;
        private TextView tempTV;


        public WeatherForeCastViewHolder(View itemView) {
            super(itemView);
            dayTV = itemView.findViewById(R.id.tv_day);
            tempTV = itemView.findViewById(R.id.tv_temp);
        }
    }
}
