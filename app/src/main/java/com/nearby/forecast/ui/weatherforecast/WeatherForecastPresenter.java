package com.nearby.forecast.ui.weatherforecast;

import android.location.Location;

import com.nearby.forecast.config.WeatherForeCastAPIConfig;
import com.nearby.forecast.config.WeatherForeCastPresenterConfig;
import com.nearby.forecast.data.network.ApiHelper;
import com.nearby.forecast.data.network.ApiHelperImpl;
import com.nearby.forecast.data.network.model.FailureType;
import com.nearby.forecast.data.network.model.WeatherForecastRequest;
import com.nearby.forecast.data.network.model.WeatherForecastResponse;
import com.nearby.forecast.locationmanager.constants.ProcessType;
import com.nearby.forecast.ui.base.BasePresenter;
import com.nearby.forecast.utils.Utils;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class WeatherForecastPresenter<V extends WeatherForecastMvpView> extends BasePresenter<V> implements WeatherForecastMvpPresenter<V> {

    /**
     * Can provide abstraction over api helper.
     */
    private ApiHelper apiHelper;
    private WeatherForeCastPresenterConfig weatherForeCastPresenterConfig;

    public WeatherForecastPresenter() {
        apiHelper = ApiHelperImpl.getApiHelper();
        weatherForeCastPresenterConfig = new WeatherForeCastPresenterConfig();
    }

    @Override
    public void retryClick() {
        if (weatherForeCastPresenterConfig.getFailureType().equals(FailureType.LOCATION)) {
            mvpView.invokeLocationManager();
        } else {
            initiateForecastReport(weatherForeCastPresenterConfig.getCurrentLocation());
        }
    }

    @Override
    public void bootStrap() {
        mvpView.showLoader();
    }

    @Override
    public void onLocationChanged(Location location) {
        mvpView.dismissLocationProgress();
        if (Utils.isCitySame(location, weatherForeCastPresenterConfig.getCurrentLocation())) {
            return;
        }
        initiateForecastReport(location);
        weatherForeCastPresenterConfig.setCurrentLocation(location);
    }

    @Override
    public void onLocationFailed(int type) {
        mvpView.dismissLocationProgress();
        mvpView.hideFailureLoader(FailureType.LOCATION);
        weatherForeCastPresenterConfig.setFailureType(FailureType.LOCATION);
    }

    @Override
    public void onProcessTypeChanged(int newProcess) {
        switch (newProcess) {
            case ProcessType.GETTING_LOCATION_FROM_GOOGLE_PLAY_SERVICES: {
                mvpView.updateLocationProgress("Getting Location from Google Play Services...");
                break;
            }
            case ProcessType.GETTING_LOCATION_FROM_GPS_PROVIDER: {
                mvpView.updateLocationProgress("Getting Location from GPS...");
                break;
            }
            case ProcessType.GETTING_LOCATION_FROM_NETWORK_PROVIDER: {
                mvpView.updateLocationProgress("Getting Location from Network...");
                break;
            }
            case ProcessType.ASKING_PERMISSIONS:
            case ProcessType.GETTING_LOCATION_FROM_CUSTOM_PROVIDER:
                break;
        }
    }

    /**
     * Fetch weather forecast and return if location is not valid
     */
    private void initiateForecastReport(Location location) {
        if (location == null) {
            weatherForeCastPresenterConfig.setFailureType(FailureType.LOCATION);
            mvpView.hideFailureLoader(FailureType.LOCATION);
            return;
        }
        mvpView.showLoader();
        apiHelper.fetchWeatherForecast(prepareWeatherRequest(location), new Callback<WeatherForecastResponse>() {
            @Override
            public void onResponse(Call<WeatherForecastResponse> call, Response<WeatherForecastResponse> response) {
                WeatherForecastResponse weatherForecastResponse = response.body();
                if (weatherForecastResponse != null) {
                    try {
                        mvpView.updateTemp(String.valueOf(weatherForecastResponse.getForecast().getForecastday().get(0).getDay().getAvgtempC()));
                        mvpView.updateCity(weatherForecastResponse.getLocation().getName());
                        mvpView.hideSuccessLoader();
                        mvpView.showBottomSheet(weatherForecastResponse);
                    } catch (Exception e) {
                        mvpView.hideFailureLoader(FailureType.NETWORK);
                        weatherForeCastPresenterConfig.setFailureType(FailureType.NETWORK);
                    }
                } else {
                    mvpView.hideFailureLoader(FailureType.NETWORK);
                    weatherForeCastPresenterConfig.setFailureType(FailureType.NETWORK);
                }
            }

            @Override
            public void onFailure(Call call, Throwable t) {
                mvpView.hideFailureLoader(FailureType.NETWORK);
                weatherForeCastPresenterConfig.setFailureType(FailureType.NETWORK);
            }
        });
    }

    /**
     * Prepare Weather Forecast API Request Body
     *
     * @return
     */
    private WeatherForecastRequest prepareWeatherRequest(Location location) {
        WeatherForecastRequest weatherForecastRequest = new WeatherForecastRequest();
        weatherForecastRequest.setCity(Utils.getCityName(location.getLatitude(), location.getLongitude()));
        weatherForecastRequest.setNumberOfDays(WeatherForeCastAPIConfig.days);
        weatherForecastRequest.setKey(WeatherForeCastAPIConfig.API_KEY);

        return weatherForecastRequest;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        apiHelper = null;
    }

}
