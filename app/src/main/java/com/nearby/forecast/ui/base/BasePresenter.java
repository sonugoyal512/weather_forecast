package com.nearby.forecast.ui.base;

/**
 * @param <V> view
 *
 *  Need to be extended by all presenter in app
 */
public class BasePresenter<V extends MvpView> implements MvpPresenter<V> {

    protected V mvpView;

    @Override
    public void onAttach(V mvpView) {
        this.mvpView = mvpView;
    }

    @Override
    public void onDetach() {
        mvpView = null;
    }
}
