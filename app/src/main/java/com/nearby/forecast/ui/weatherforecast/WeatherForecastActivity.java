package com.nearby.forecast.ui.weatherforecast;

import android.app.ProgressDialog;
import android.location.Location;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.nearby.forecast.R;
import com.nearby.forecast.constants.AppConstants;
import com.nearby.forecast.data.network.model.FailureType;
import com.nearby.forecast.data.network.model.WeatherForecastResponse;
import com.nearby.forecast.locationmanager.base.LocationBaseActivity;
import com.nearby.forecast.locationmanager.configuration.Configurations;
import com.nearby.forecast.locationmanager.configuration.LocationConfiguration;
import com.nearby.forecast.ui.weatherforecast.bottomsheet.ForeCastBottomSheet;
import com.nearby.forecast.utils.AnimationUtility;

public class WeatherForecastActivity extends LocationBaseActivity implements WeatherForecastMvpView, View.OnClickListener {

    private TextView tempInCelsiusTV;

    private TextView cityNameTV;

    private RelativeLayout foreCastWrapperSuccessRL;

    private ImageView loaderIV;

    private RelativeLayout foreCastWrapperFailureRL;

    private TextView failedStatusTV;

    private ProgressDialog progressDialog;

    private WeatherForecastMvpPresenter<WeatherForecastMvpView> presenter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        try {
            this.getSupportActionBar().hide();
        } catch (NullPointerException e) {
        }
        setContentView(R.layout.activity_weather_forecast);
        initUI();
        initPresenter();
        getLocation();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (getLocationManager().isWaitingForLocation()
                && !getLocationManager().isAnyDialogShowing()) {
            displayProgress();
        }
    }

    private void initPresenter() {
        presenter = new WeatherForecastPresenter<>();
        presenter.onAttach(this);
        presenter.bootStrap();
    }

    private void displayProgress() {
        if (progressDialog == null) {
            progressDialog = new ProgressDialog(this);
            progressDialog.getWindow().addFlags(Window.FEATURE_NO_TITLE);
            progressDialog.setMessage("Getting location...");
        }

        if (!progressDialog.isShowing()) {
            progressDialog.show();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        dismissLocationProgress();
    }

    private void initUI() {
        tempInCelsiusTV = findViewById(R.id.tv_temp);
        cityNameTV = findViewById(R.id.tv_city_name);
        foreCastWrapperSuccessRL = findViewById(R.id.rl_forecast_wrapper);
        foreCastWrapperFailureRL = findViewById(R.id.rl_failure_wrapper);
        loaderIV = findViewById(R.id.iv_loader);
        failedStatusTV = findViewById(R.id.tv_some_thing_went_wrong);
        findViewById(R.id.retry_button).setOnClickListener(this);
    }

    @Override
    public void showLoader() {
        loaderIV.setVisibility(View.VISIBLE);
        AnimationUtility.rotateViewAntiClockWise(loaderIV, getApplicationContext());
        foreCastWrapperSuccessRL.setVisibility(View.GONE);
        foreCastWrapperFailureRL.setVisibility(View.GONE);
    }

    @Override
    public void hideFailureLoader(FailureType failureType) {
        loaderIV.clearAnimation();
        loaderIV.setVisibility(View.GONE);
        foreCastWrapperFailureRL.setVisibility(View.VISIBLE);
        foreCastWrapperSuccessRL.setVisibility(View.GONE);
        if (failureType.equals(FailureType.LOCATION)) {
            failedStatusTV.setText(getString(R.string.something_went_wrong_at_your_end));
        } else {
            failedStatusTV.setText(getString(R.string.something_went_wrong_at_our_end));
        }
    }

    @Override
    public void hideSuccessLoader() {
        loaderIV.clearAnimation();
        loaderIV.setVisibility(View.GONE);
        foreCastWrapperFailureRL.setVisibility(View.GONE);
        foreCastWrapperSuccessRL.setVisibility(View.VISIBLE);
    }

    @Override
    public void updateTemp(String temp) {
        tempInCelsiusTV.setText(temp);
    }

    @Override
    public void updateCity(String city) {
        cityNameTV.setText(city);
    }

    @Override
    public void showBottomSheet(WeatherForecastResponse weatherForecastResponse) {
        ForeCastBottomSheet foreCastBottomSheet = new ForeCastBottomSheet();
        Bundle bundle = new Bundle();
        bundle.putSerializable(AppConstants.FORECAST_JSON, weatherForecastResponse);
        foreCastBottomSheet.setArguments(bundle);
        foreCastBottomSheet.setCancelable(false);
        foreCastBottomSheet.show(getSupportFragmentManager(), foreCastBottomSheet.getTag());
    }

    @Override
    public void updateLocationProgress(String text) {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.setMessage(text);
        }
    }

    @Override
    public void dismissLocationProgress() {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }
    }

    @Override
    public void invokeLocationManager() {
        getLocation();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        presenter.onDetach();
    }

    @Override
    public void onClick(View view) {
        if (view.getId() == R.id.retry_button) {
            presenter.retryClick();
        }
    }

    @Override
    public LocationConfiguration getLocationConfiguration() {
        return Configurations.defaultConfiguration("Give me the GPS permission!", "Would you mind to turn GPS on?");
    }

    @Override
    public void onLocationChanged(Location location) {
        presenter.onLocationChanged(location);
    }

    @Override
    public void onLocationFailed(int type) {
        presenter.onLocationFailed(type);
    }
}
