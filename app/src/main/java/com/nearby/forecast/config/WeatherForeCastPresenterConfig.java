package com.nearby.forecast.config;

import android.location.Location;

import com.nearby.forecast.data.network.model.FailureType;

public class WeatherForeCastPresenterConfig {

    private FailureType failureType;
    private Location currentLocation;

    public FailureType getFailureType() {
        return failureType;
    }

    public void setFailureType(FailureType failureType) {
        this.failureType = failureType;
    }


    public Location getCurrentLocation() {
        return currentLocation;
    }

    public void setCurrentLocation(Location currentLocation) {
        this.currentLocation = currentLocation;
    }
}
