package com.nearby.forecast.locationmanager.listener;

public interface DialogListener {

    void onPositiveButtonClick();

    void onNegativeButtonClick();

}
