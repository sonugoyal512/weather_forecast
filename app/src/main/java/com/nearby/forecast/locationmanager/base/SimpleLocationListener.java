package com.nearby.forecast.locationmanager.base;

import android.location.Location;
import android.os.Bundle;

import com.nearby.forecast.locationmanager.constants.ProcessType;
import com.nearby.forecast.locationmanager.listener.LocationListener;


/**
 * need to be overridden.
 */
public abstract class SimpleLocationListener implements LocationListener {

    @Override
    public void onProcessTypeChanged(@ProcessType int processType) {

    }

    @Override
    public void onPermissionGranted(boolean alreadyHadPermission) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

}
