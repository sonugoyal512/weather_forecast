package com.nearby.forecast.locationmanager.listener;

public interface FallbackListener {
    void onFallback();
}