package com.nearby.forecast.utils;

import android.content.Context;
import android.view.View;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;

import com.nearby.forecast.R;

public class AnimationUtility {


    public static void rotateViewAntiClockWise(View view, Context context) {
        Animation aniRotate = AnimationUtils.loadAnimation(context, R.anim.anti_clock_wise_rotation);
        view.startAnimation(aniRotate);
    }

}
