package com.nearby.forecast.utils;

import android.content.Context;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.util.DisplayMetrics;

import com.nearby.forecast.WeatherForeCastApplication;

import java.util.List;

public class Utils {

    public static float convertPixelsToDp(float px, Context context) {
        return px / ((float) context.getResources().getDisplayMetrics().densityDpi / DisplayMetrics.DENSITY_DEFAULT);
    }

    /**
     * @param lat
     * @param lng
     * @return CityName Default City Noida
     */
    public static String getCityName(double lat, double lng) {
        Geocoder geocoder = WeatherForeCastApplication.getGeocoderInstance();
        String cityName = "gurgaon";
        List<Address> addresses = null;
        try {
            addresses = geocoder.getFromLocation(lat, lng, 1);
            cityName = addresses.get(0).getAddressLine(0);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return cityName;
    }

    public static boolean isCitySame(Location l1, Location l2) {
        if (l1 == null || l2 == null) {
            return false;
        }
        return getCityName(l1.getLatitude(), l1.getLongitude()).equalsIgnoreCase(getCityName(l2.getLatitude(), l2.getLongitude()));
    }
}
