package com.nearby.forecast.utils;

import java.util.Calendar;
import java.util.Date;
import java.util.Locale;

public class DateUtils {

    public static String getWeekDay(long epochTime) {
        Date date = new Date(epochTime * 1000);
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.get(Calendar.DAY_OF_WEEK);
        return c.getDisplayName(Calendar.DAY_OF_WEEK, Calendar.LONG, Locale.getDefault());
    }
}
