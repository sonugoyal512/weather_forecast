# Android Weather Application


## Installation
Clone this repository and import into **Android Studio**
https://gitlab.com/sonugoyal512/weather_forecast.git


## Maintainers
This project is mantained by:
Sonu Goel


## Contributing

1. Fork it
2. Create your feature branch (git checkout -b my-new-feature)
3. Commit your changes (git commit -m 'Add some feature')
4. Run the linter (ruby lint.rb').
5. Push your branch (git push origin my-new-feature)
6. Create a new Pull Request

## About
This project shows the weather forecast for current day and next coming four days.
Coming days are configurable.
Using the public api of apixu for weather forecast